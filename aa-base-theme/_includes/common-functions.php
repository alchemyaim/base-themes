<?php
/*---------------------------------
	Theme functions
------------------------------------*/
require_once( 'functions/admin.php' ); // Customized admin features
require_once( 'functions/editor.php' ); // Visual editor customizations
require_once( 'functions/theme-functions.php' ); // Functions used in the theme to be called directly
require_once( 'functions/theme-hooks.php' ); // Hooks tied to the theme, these shouldn't be called directly

//Enable ACF Gutenburg Block Builder
if ( function_exists( 'acf_add_options_page' ) && !empty( get_option( 'options_use_gutenburg_block_builder' ) ) ) { 
	require_once( 'functions/acf-block-functions.php' ); // Add functionality for ACF Gutenburg Block Builder
	wp_enqueue_style('block-styles', get_template_directory_uri() .  '/_compiled-styles/blocks-min.css', FALSE);
 	//Enqueue block editor style
	function gutenberg_editor_assets() {
	  // Load the theme styles within Gutenberg.
	  wp_enqueue_style('gutenberg-editor-styles', get_template_directory_uri() .  '/_compiled-styles/gutenberg-editor-styles-min.css', FALSE);
	}
	add_action('enqueue_block_editor_assets', 'gutenberg_editor_assets');
} else {
	add_action('admin_head', 'hide_gutenburg_block_builder');
	function hide_gutenburg_block_builder() {
  	echo '<style>
    	#acf-group_5c9d327d28a8f{display:none !important;} 
  		</style>';
	}
}

//Enable WooCommerce functions ONLY if WooCommerce exists.
if ( in_array( 'woocommerce/woocommerce.php', 
    apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) 
  )) {
			
		require_once( 'functions/woocommerce.php' ); // Enable woocommerce support, and custom woocommerce functions
		wp_enqueue_style('woo-styles', get_template_directory_uri() .  '/_compiled-styles/woocommerce-min.css', FALSE);
	
		function create_sidebars() {
			register_sidebar( array(
				'name' => 'Store',
				'id' => 'store_sidebar',
				'description' => 'WooCommerce store sidebar.',

				'before_widget' => '<div id="%1$s" class="widget sidebox %2$s">',
				'after_widget' => '</div>',
				'before_title' => '<h3 class="widget-title">',
				'after_title' => '</h3>',
			));
			register_sidebar( array(
				'name' => 'Checkout',
				'id' => 'checkout_sidebar',
				'description' => 'WooCommerce checkout page sidebar.',

				'before_widget' => '<div id="%1$s" class="widget sidebox %2$s">',
				'after_widget' => '</div>',
				'before_title' => '<h3 class="widget-title">',
				'after_title' => '</h3>',
			));
		}
		add_action( 'after_setup_theme', 'create_sidebars' );
}

//Enable Flex Template
if ( function_exists( 'acf_add_options_page' ) &&  get_field( 'use_flex_layout', 'option' ) == 1 ) { 
 	require_once( 'functions/flex-layout-functions.php' ); // Add functionality for Radley's flex layout system
	wp_enqueue_style('flex-layout-styles', get_template_directory_uri() .  '/_compiled-styles/flex-layout-min.css', FALSE);
} else {
	function remove_flex_page_template() {
    global $pagenow;
    if ( in_array( $pagenow, array( 'post-new.php', 'post.php') ) && get_post_type() == 'page' ) { ?>
        <script type="text/javascript">
            (function($){
                $(document).ready(function(){
                    $('#page_template option[value="_templates/template_flex_layout_page.php"]').remove();
                })
            })(jQuery)
        </script>
    <?php 
    }
}
add_action('admin_footer', 'remove_flex_page_template', 10);
}

/*---------------------------------
	Widgets
------------------------------------*/
if ( function_exists( 'acf_add_options_page' ) && get_field( 'use_social_links', 'option' ) == 1 ) {
	require_once( 'widgets/social-widget.php' );
}
/*---------------------------------------------
	Custom Post Types
------------------------------------------------*/
// require_once( 'cpts/custom-post-type-template.php' );

/*---------------------------------------------
	Advanced Custom Fields: Shortcode Support
------------------------------------------------*/
//Add shortcode support to ACF text, textarea & medium editor fields
add_filter('acf/format_value/type=medium_editor', 'do_shortcode');
add_filter('acf/format_value/type=textarea', 'do_shortcode');
add_filter('acf/format_value/type=text', 'do_shortcode');

/*---------------------------------------------
	Advanced Custom Fields: Theme Settings
------------------------------------------------*/

if ( function_exists( 'acf_add_options_page' ) ) {
	$user = wp_get_current_user();
	$user = $user->user_login;
	
	acf_add_options_page( array(
		'page_title' => 'Theme General Settings',
		'menu_title' => 'Theme Settings',
		'menu_slug'  => 'theme-general-settings',
		'capability' => 'edit_posts',
		'redirect'   => false,
	) );
	
	if ( $user == 'alchemyandaim' || $user == 'wpsapphire' ):
		acf_add_options_sub_page( array(
			'page_title'  => 'Theme Developer Page',
			'menu_title'  => 'Developer',
			'parent_slug' => 'theme-general-settings',
		) );
	
	endif;
	
	if($user != 'alchemyandaim' && $user != 'wpsapphire'): 	
		add_filter('acf/settings/show_admin', '__return_false');
	endif;
	
	if($user != 'alchemyandaim'  || $user != 'wpsapphire'):
	//Hide Advanced Tab in Flexlayout
	add_action('admin_head', 'hide_advanced_tab');

	function hide_advanced_tab() {
  	echo '<style>
    	.acf-field-5b243d974f1b6{display:none;} 
  		</style>';
	}
	endif;
}
/*--------------------------------------------------------------------
   Admin notice to warn that WP SCSS is set to recompile every visit
----------------------------------------------------------------------*/

if ( defined('WP_SCSS_ALWAYS_RECOMPILE') && WP_SCSS_ALWAYS_RECOMPILE ) {
	function aa_warn_scss_recompile() {
		?>
		<div class="notice notice-error">
			<p><strong>A+A Reminder:</strong> The plugin WP-SCSS is set to compile on every page load. This can have high impact on the loading speed of the website.</p>
			<p>To disable this, edit wp-config.php and remove or disable:</p>
			<p><code class="code">define('WP_SCSS_ALWAYS_RECOMPILE', true);</code></p>
		</div>
		<?php
	}
	add_action( 'admin_notices', 'aa_warn_scss_recompile' );
}

/*-------------------------------------------------------
	Add custom dashboard widgets defined in ACF
----------------------------------------------------------*/

/* ACCORDION WIDGET */
function aa_accordion_dashboard_widget() {
	if ( !function_exists( 'acf_add_options_page' ) ) return;
global $wp_meta_boxes;
	
$accordion = get_field( 'accordion_dashboard_widget', 'option' );
$widget_title = get_field( 'accordion_dashboard_widget_title', 'option' );
	
	if($accordion == 1){
 
wp_add_dashboard_widget('accordion_dashboard_widget', $widget_title, 'custom_dashboard_help');
}


function custom_dashboard_help() { 
	
	if ( have_rows( 'accordion_widget', 'option' ) ) : 
	while ( have_rows( 'accordion_widget', 'option' ) ) : the_row(); 
	
		//vars
		$label = get_sub_field( 'accordion_label' ); 
		$content = get_sub_field( 'accordion_content' );
	
	echo '<div class="accordion">'.$label.'</div>';
	echo '<div class="panel">'.$content.'</div>';
	
	 endwhile; 
 else : 
	// no rows found 
endif; 

	echo '<script>
	var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  });
}
</script>';
 }
	}

add_action('wp_dashboard_setup', 'aa_accordion_dashboard_widget');

/* STANDARD WIDGET */
function aa_dashboard_widget() {
	if ( !function_exists( 'acf_add_options_page' ) ) return;
	
	if ( have_rows( 'dashboard_widgets', 'options' ) ):
		while( have_rows( 'dashboard_widgets', 'options' ) ): the_row();
			
			// vars
			$title = get_sub_field( 'widget_title' );
			$name_clean = sanitize_title( $title );
			$the_msg = get_sub_field( 'widget_content' );
			
			wp_add_dashboard_widget(
				$name_clean . '_dashboard_widget',     // Widget slug
				$title,                              // Title
				'dashboard_widget_display_function', // Display function
				'dashboard_widget_control_function', // Control function
				array(
					'title'   => $title,
					'slug'    => $name_clean,
					'content' => $the_msg,
				) // Callback arguments
			);
		
		endwhile;
	endif;
}

add_action( 'wp_dashboard_setup', 'aa_dashboard_widget' );

function dashboard_widget_display_function( $post_obj, $args ) {
	printf( $args['args']['content'] );
}