<?php
add_action('acf/init', 'base_acf_blocks_init');
function base_acf_blocks_init() {
	
	// Variable to Theme Directory
	$dir = get_template_directory_uri();

    // Check function exists.
    if( function_exists('acf_register_block_type') ) {

        	// register a general content block.
		   acf_register_block_type(array(
			  'name'              => 'content',
			  'title'             => __('ACF General Content Block'),
			  'description'       => __('An ACF general content block.'),
			  'render_callback'	=> 'my_acf_block_render_callback',
			  //'render_template'   => 'template-parts/blocks/testimonial/testimonial.php',
			  'category'          => 'aa-blocks',
			  'icon'              => '<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 272.1 320" style="enable-background:new 0 0 272.1 320;" xml:space="preserve">
<g>
	<path d="M0,159.7c0-44.6,0-89.3,0-133.9c0-13,7-22.5,18.6-25.2C20.9,0,23.4,0,25.7,0c63.3,0,126.5,0,189.8,0
		c15.6,0,25.7,10,25.7,25.5c0,78.8,0,157.6,0,236.3c0,6.3-2.7,9.9-7.6,10.1c-4.1,0.2-7.4-2.2-8.1-6.2c-0.4-1.8-0.4-3.7-0.4-5.6
		c0-77.3,0-154.6,0-231.8c0-1.6,0-3.3-0.1-4.9c-0.4-4.3-3.3-7.1-7.5-7.4c-1-0.1-2,0-3,0c-62.5,0-125,0-187.5,0
		c-8.3,0-10.8,2.4-10.8,10.6c0,88.9,0,177.8,0,266.7c0,8.3,2.4,10.7,10.7,10.7c72.8,0,145.5,0,218.3,0c8.5,0,11.1-2.6,11.1-11.3
		c0-35.3,0-70.5,0-105.8c0-7.2,2.7-10.9,7.8-11c5.3-0.1,8.1,3.8,8.1,11.1c0,35.6,0,71.3,0,106.9c0,16.1-9.8,25.9-25.9,26
		c-73.4,0-146.8,0-220.2,0c-16.2,0-26-9.9-26-26.3C0,249,0,204.4,0,159.7z"/>
	<path d="M119.8,117.9c-20.6,0-41.2,0-61.9,0c-11.5,0-17.8-6.3-17.8-17.9c0-11,0-22,0-33c0.1-10.6,6.5-17.2,17-17.2
		c41.9-0.1,83.7,0,125.6,0c10.7,0,17.3,6.7,17.3,17.3c0,11.3,0,22.5,0,33.8c0,10.2-6.7,16.9-16.9,17
		C162.1,118,140.9,117.9,119.8,117.9z M183.9,66.3c-42.8,0-85.2,0-127.6,0c0,12,0,23.7,0,35.5c42.6,0,85,0,127.6,0
		C183.9,89.8,183.9,78.2,183.9,66.3z"/>
	<path d="M119.8,216c-22.7,0-45.5,0-68.2,0c-1,0-2,0-3,0c-5.2-0.3-8.4-3.5-8.4-8.2c0-4.5,3.2-7.6,8.2-7.9c0.9-0.1,1.7,0,2.6,0
		c46,0,92,0,137.9,0c7.3,0,11.2,2.9,11.1,8.2c-0.1,5.2-3.8,8-10.9,8C166,216,142.9,216,119.8,216z"/>
	<path d="M120.1,151.9c-23.5,0-47,0-70.5,0c-5.7,0-9-2.5-9.4-7c-0.4-4.6,2.6-8.4,7.2-8.9c1.2-0.1,2.5-0.1,3.7-0.1
		c46,0,91.9,0,137.9,0c1.1,0,2.3-0.1,3.4,0c4.5,0.4,7.8,3.9,7.6,8.1c-0.1,4.2-3.3,7.5-7.7,7.7c-5.7,0.2-11.5,0.1-17.2,0.1
		C156.8,151.9,138.5,151.9,120.1,151.9z"/>
	<path d="M120.2,168c23.2,0,46.5,0,69.7,0c2.1,0,4.4,0.2,6.1,1.1c3.2,1.6,4.6,4.6,3.9,8.2c-0.7,3.5-3,5.7-6.5,6.5
		c-0.8,0.2-1.7,0.1-2.6,0.1c-47.2,0-94.4,0-141.7,0c-5.6,0-9.1-3.3-9-8.3c0.1-4,2.9-7.1,6.9-7.5c1.4-0.2,2.7-0.1,4.1-0.1
		C74.2,168,97.2,168,120.2,168z"/>
	<path d="M87.9,247.9c-13,0-26,0-39,0c-4.7,0-7.8-2.4-8.6-6.2c-1-5.1,2.5-9.5,7.8-9.6c5.6-0.2,11.3,0,16.9,0c20,0,40,0,60,0
		c1,0,2,0,3,0c4.8,0.3,8.2,3.7,8.1,8.2c-0.1,4.3-3.4,7.7-8,7.7C114.7,248,101.3,247.9,87.9,247.9C87.9,247.9,87.9,247.9,87.9,247.9z
		"/>
</g>
</svg>',
			  'keywords'          => array( 'general content', 'wysiwyg', 'acf' ),
			  'example' => array(
				'attributes' => array(
					'mode' => 'preview',
					'data' => array(
						'gutenberg_preview_img' => __('<img src="'.$dir.'/_static/images/block-previews/content-block.png">'),
					),
				)
			),
			   'supports' => array(
               'align' => true,
			   'color' => true,
               'anchor' => true, 
               'customClassName' => true, 
               'layout' => false, 
               'spacing' => array(
                  'margin' => true, 
                  'padding' => true, 
                  'blockGap' => array(
                     'horizontal', 
                     'vertical' 
                  )
               ), 
               'typography' => array(
                        'fontSize' => true, 
                        'lineHeight' => true, 
                        'FontFamily' => true, 
                        'FontWeight' => true, 
                        'FontStyle' => true, 
                        'TextTransform' => true, 
                        'LetterSpacing' => true, 
                        '__experimentalFontFamily' => true, 
                        '__experimentalFontWeight' => true, 
                        '__experimentalFontStyle' => true, 
                        '__experimentalTextTransform' => true, 
                        '__experimentalLetterSpacing' => true 
                     )
            ),
		   ));

			// register a accordion block.
		   acf_register_block_type(array(
			  'name'              => 'accordion',
			  'title'             => __('ACF Accordion Content Block'),
			  'description'       => __('An ACF accordion content block that is accessible.'),
			  'render_callback'	=> 'my_acf_block_render_callback', 
			  //'render_template'   => 'template-parts/blocks/landing/page_landing.php',
			  'category'          => 'aa-blocks',
			  'icon'              => '<svg width="800px" height="800px" viewBox="0 0 17 17" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
	<path d="M4 0v9h13v-9h-13zM16 8h-11v-7h11v7zM0 3h3v-3h-3v3zM1 1h1v1h-1v-1zM4 13h13v-3h-13v3zM5 11h11v1h-11v-1zM0 13h3v-3h-3v3zM1 11h1v1h-1v-1zM4 17h13v-3h-13v3zM5 15h11v1h-11v-1zM0 17h3v-3h-3v3zM1 15h1v1h-1v-1z" fill="#000000" />
</svg>',
			  'keywords'          => array( 'accordion', 'faq', 'accessible', 'acf' ),
			   'example' => array(
				'attributes' => array(
					'mode' => 'preview',
					'data' => array(
						'gutenberg_preview_img' => __('<img src="'.$dir.'/_static/images/block-previews/accordion-block.jpg">'),
					),
				)
			),
			   'supports' => array(
               'align' => true,
			   'color' => true,
               'anchor' => true, 
               'customClassName' => true, 
               'layout' => false, 
               'spacing' => array(
                  'margin' => true, 
                  'padding' => true, 
                  'blockGap' => array(
                     'horizontal', 
                     'vertical' 
                  )
               ), 
               'typography' => array(
                        'fontSize' => true, 
                        'lineHeight' => true, 
                        'FontFamily' => true, 
                        'FontWeight' => true, 
                        'FontStyle' => true, 
                        'TextTransform' => true, 
                        'LetterSpacing' => true, 
                        '__experimentalFontFamily' => true, 
                        '__experimentalFontWeight' => true, 
                        '__experimentalFontStyle' => true, 
                        '__experimentalTextTransform' => true, 
                        '__experimentalLetterSpacing' => true 
                     )
            ),
		   ));
		
			// register a popup block.
		   acf_register_block_type(array(
			  'name'              => 'popup',
			  'title'             => __('ACF Popup Dialogue Content Block'),
			  'description'       => __('An ACF popup dialogue content block that is accessible.'),
			  'render_callback'	=> 'my_acf_block_render_callback', 
			  //'render_template'   => 'template-parts/blocks/landing/page_landing.php',
			  'category'          => 'aa-blocks',
			  'icon'              => '<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 412 410" style="enable-background:new 0 0 412 410;" xml:space="preserve">
<g>
	<path d="M102.7,152.9c0-33.2,0.5-66.4-0.1-99.5C102,22.9,125.6-0.3,156.1,0c59.3,0.5,118.7,0.1,178,0.2c10.5,0,21.1-0.5,31.4,0.7
		c25.4,3.1,44.3,24.4,44.4,49.9c0.2,68.9,0.2,137.7,0,206.6c-0.1,26.8-22,49.1-48.8,49.9c-20,0.6-40,0.2-60,0.2
		c-49.3,0-98.7,0.1-148-0.1c-22.9-0.1-42.3-15.1-48.3-37.1c-1.6-5.8-2-12.2-2-18.3C102.6,218.9,102.7,185.9,102.7,152.9z M358.8,256
		c0-2.3,0-4.1,0-5.9c0-64.3,0-128.7,0.1-193c0-4.6-1.2-5.9-5.8-5.9c-64.5,0.2-129,0.1-193.5,0.1c-1.8,0-3.5,0-5.3,0
		c0,68.6,0,136.6,0,204.7C222.4,256,290.2,256,358.8,256z"/>
	<path d="M204.8,358.8c0,17.3,0,34,0,51.3c-1.7,0-3.2,0-4.6,0c-49,0-98,0.1-147,0c-25.5,0-45.8-15.7-51.5-39.7
		c-0.9-3.7-1.4-7.6-1.4-11.4c-0.1-50.2-0.1-100.4-0.1-150.6c0-0.8,0.1-1.6,0.2-2.9c16.8,0,33.5,0,51.1,0c0,1.9,0,4,0,6.1
		c0,47,0.1,94-0.1,141.1c0,5.2,1.5,6.3,6.4,6.3c47-0.2,94-0.1,141-0.1C200.6,358.8,202.6,358.8,204.8,358.8z"/>
</g>
</svg>',
			  'keywords'          => array( 'popup', 'modal', 'dialogue', 'accessible', 'acf' ),
			   'example' => array(
				'attributes' => array(
					'mode' => 'preview',
					'data' => array(
						'gutenberg_preview_img' => __('<img src="'.$dir.'/_static/images/block-previews/popup-block.jpg">'),
					),
				)
			),
			   'supports' => array(
               'align' => true,
			   'color' => true,
               'anchor' => true, 
               'customClassName' => true, 
               'layout' => false, 
               'spacing' => array(
                  'margin' => true, 
                  'padding' => true, 
                  'blockGap' => array(
                     'horizontal', 
                     'vertical' 
                  )
               ), 
               'typography' => array(
                        'fontSize' => true, 
                        'lineHeight' => true, 
                        'FontFamily' => true, 
                        'FontWeight' => true, 
                        'FontStyle' => true, 
                        'TextTransform' => true, 
                        'LetterSpacing' => true, 
                        '__experimentalFontFamily' => true, 
                        '__experimentalFontWeight' => true, 
                        '__experimentalFontStyle' => true, 
                        '__experimentalTextTransform' => true, 
                        '__experimentalLetterSpacing' => true 
                     )
            ),
		   ));
		
		// register a slider block.
		   acf_register_block_type(array(
			  'name'              => 'slider',
			  'title'             => __('ACF Slider Content Block'),
			  'description'       => __('An ACF slider content block that is accessible.'),
			  'render_callback'	=> 'my_acf_block_render_callback', 
			  //'render_template'   => 'template-parts/blocks/landing/page_landing.php',
			  'category'          => 'aa-blocks',
			  'icon'              => '<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 235 175" style="enable-background:new 0 0 235 175;" xml:space="preserve">
<g>
	<g>
		<g>
			<path d="M-1,89v88h118h118V89V1H117H-1V89z M215.6,89v68.2H117H18.4V89V20.8H117h98.6V89z"/>
			<path d="M56.2,73.2C49.3,81.7,43.8,89,43.9,89.3c0.1,0.3,5.8,7.4,12.5,15.8c8.5,10.5,12.4,15.1,12.8,14.8
				c4.1-3.1,14.6-11.7,14.6-11.9c0-0.1-3.3-4.4-7.4-9.4C72.3,93.5,69,89.2,69,89c0-0.2,2.8-3.8,6.1-7.9c3.3-4.1,6.7-8.4,7.6-9.4
				l1.4-1.8L77,64.2c-4-3.2-7.5-5.9-7.7-6.1C68.9,57.8,64.5,62.8,56.2,73.2z"/>
			<path d="M157.6,63.6c-4.1,3.3-7.5,6.1-7.5,6.4c0,0.2,3.3,4.4,7.2,9.3c4,4.9,7.3,9.2,7.4,9.5c0.1,0.3-3.1,4.7-7.1,9.8
				c-4.1,5.1-7.4,9.4-7.4,9.5c0,0.2,13.4,11.2,14.7,12c0.4,0.3,24.7-29.2,25.3-30.7c0.1-0.4-21.7-28.3-24.4-31.1
				C165.2,57.7,163.7,58.8,157.6,63.6z"/>
		</g>
	</g>
</g>
</svg>',
			  'keywords'          => array( 'slider', 'accessible', 'acf' ),
			   'example' => array(
				'attributes' => array(
					'mode' => 'preview',
					'data' => array(
						'gutenberg_preview_img' => __('<img src="'.$dir.'/_static/images/block-previews/slider-block.jpg">'),
					),
				)
			),
			   'supports' => array(
               'align' => true,
			   'color' => true,
               'anchor' => true, 
               'customClassName' => true, 
               'layout' => false, 
               'spacing' => array(
                  'margin' => true, 
                  'padding' => true, 
                  'blockGap' => array(
                     'horizontal', 
                     'vertical' 
                  )
               ), 
               'typography' => array(
                        'fontSize' => true, 
                        'lineHeight' => true, 
                        'FontFamily' => true, 
                        'FontWeight' => true, 
                        'FontStyle' => true, 
                        'TextTransform' => true, 
                        'LetterSpacing' => true, 
                        '__experimentalFontFamily' => true, 
                        '__experimentalFontWeight' => true, 
                        '__experimentalFontStyle' => true, 
                        '__experimentalTextTransform' => true, 
                        '__experimentalLetterSpacing' => true 
                     )
            ),
		   ));
		
		// register a tabs block.
		   acf_register_block_type(array(
			  'name'              => 'tabs',
			  'title'             => __('ACF Tab Content Block'),
			  'description'       => __('An ACF tab content block that is accessible.'),
			  'render_callback'	=> 'my_acf_block_render_callback', 
			  //'render_template'   => 'template-parts/blocks/landing/page_landing.php',
			  'category'          => 'aa-blocks',
			  'icon'              => '<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 513 474" style="enable-background:new 0 0 513 474;" xml:space="preserve">
<g>
	<path d="M1,421C1,298.3,1,175.6,1,53c0.3-1.1,0.8-2.1,1-3.2C7.7,21.5,31.9,1.1,60.4,1.1C175.8,1,291.2,1,406.6,1.1
		c27,0,51.2,18.8,57.1,45.1c2.3,10.2,1.8,21.1,2.1,31.7c0.1,3.3,0.8,4.9,4.3,5.8c13.6,3.9,24.3,12.1,32.4,23.6
		c5.7,8.1,8.3,17.3,10.4,26.7c0,95.7,0,191.4,0,287c-0.4,1.1-0.8,2.1-1,3.2c-5.7,28.1-29.7,48.7-58,48.7c-131.3,0.1-262.7,0.1-394,0
		c-19.3,0-34.9-8.6-46.9-23.8C6.6,440.8,3.6,431,1,421z M165,121c0-2.1,0-3.7,0-5.3c0-17,0-34,0-51c0-16-7.6-23.7-23.4-23.7
		c-25.7,0-51.3,0-77,0C48.6,41,41,48.5,41,64.5c0,115,0,230,0,345c0,16,7.6,23.5,23.6,23.5c128.3,0,256.6,0,385,0
		c15.9,0,23.5-7.6,23.5-23.6c0-88.2,0-176.3,0-264.5c0-16.4-7.4-23.9-23.7-23.9c-92.7,0-185.3,0-278,0C169.4,121,167.4,121,165,121z
		 M296,80.7c0-6.1,0-11.9,0-17.7c0-13.9-8.1-22-22-22.1c-22.3,0-44.6,0-67,0c-1.6,0-3.2,0-3.9,0c0.9,13.5,1.7,26.5,2.6,39.8
		C235.4,80.7,265.3,80.7,296,80.7z M426,80.7c0-6.1,0-11.9,0-17.7c0-13.9-8.2-22-22-22.1c-22,0-44,0-66,0c-1.6,0-3.2,0-3.9,0
		c0.9,13.5,1.7,26.5,2.6,39.8C366.1,80.7,395.7,80.7,426,80.7z"/>
</g>
</svg>',
			  'keywords'          => array( 'tabs', 'tab', 'navigation', 'nav', 'accessible', 'acf' ),
			   'example' => array(
				'attributes' => array(
					'mode' => 'preview',
					'data' => array(
						'gutenberg_preview_img' => __('<img src="'.$dir.'/_static/images/block-previews/tabs-block.jpg">'),
					),
				)
			),
			   'supports' => array(
               'align' => true,
			   'color' => true,
               'anchor' => true, 
               'customClassName' => true, 
               'layout' => false, 
               'spacing' => array(
                  'margin' => true, 
                  'padding' => true, 
                  'blockGap' => array(
                     'horizontal', 
                     'vertical' 
                  )
               ), 
               'typography' => array(
                        'fontSize' => true, 
                        'lineHeight' => true, 
                        'FontFamily' => true, 
                        'FontWeight' => true, 
                        'FontStyle' => true, 
                        'TextTransform' => true, 
                        'LetterSpacing' => true, 
                        '__experimentalFontFamily' => true, 
                        '__experimentalFontWeight' => true, 
                        '__experimentalFontStyle' => true, 
                        '__experimentalTextTransform' => true, 
                        '__experimentalLetterSpacing' => true 
                     )
            ),
		   ));
		
		
		
		
    }
}
?>