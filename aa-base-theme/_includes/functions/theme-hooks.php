<?php

// Include html in the <head> element. Do not put CSS or JS here unless absolutely necessary (like the ie<9 shim)
function aa_theme_head() {
	?><meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	
	<link rel="alternate" type="application/rss+xml" title="<?php bloginfo( 'name' ); ?> RSS Feed" href="<?php bloginfo( 'rss2_url' ); ?>" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	
	<!-- Legacy IE Support For HTML5 Tags -->
	<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
	<?php
	if ( function_exists('get_field') ) {
		// Favicon
		if ( $img = get_field( 'site_favicon', 'option' ) ) {
			echo '<link rel="shortcut icon" type="image/x-icon" href="', esc_attr($img['url']), '" />' . "\n";
		}else{
			echo '<link rel="shortcut icon" type="image/x-icon" href="https://alchemyandaim.com/wp-content/uploads/2016/06/favicon.png' . '" />' . "\n";
		}
		
		// Apple Touch Icons
		if ( $img = get_field( 'iphone', 'option' ) ) {
			echo '<link rel="apple-touch-icon-precomposed" sizes="57x57" href="', esc_attr($img['url']), '">' . "\n";
		}
		if ( $img = get_field( 'iphone_retina', 'option' ) ) {
			echo '<link rel="apple-touch-icon-precomposed" sizes="114x114" href="', esc_attr($img['url']), '">' . "\n";
		}
		if ( $img = get_field( 'ipad', 'option' ) ) {
			echo '<link rel="apple-touch-icon-precomposed" sizes="72x72" href="', esc_attr($img['url']), '">' . "\n";
		}
		if ( $img = get_field( 'ipad_retina', 'option' ) ) {
			echo '<link rel="apple-touch-icon-precomposed" sizes="144x144" href="', esc_attr($img['url']), '">' . "\n";
		}
		
		// Google Analytics via ACF
		if ( get_field( 'google_analytics_location', 'option' ) == "header" ) {
			echo get_field( 'google_analytics_code', 'option' ) . "\n";
		}
		
		// Header Code
		if ( get_field( 'add_site_header_code', 'option' ) == 1 ) { 
 			echo get_field( 'header_code', 'option' ) . "\n"; 
		} 
	}
	
	if ( is_singular() && comments_open() ) {
		wp_enqueue_script( 'comment-reply' );
	}
	
	echo "\n";
}
add_action( 'wp_head', 'aa_theme_head', 0 );

// Move W3TC minified CSS to the end of the <head> to improve specificity and fix other plugins or external stylesheets from overwriting our theme CSS.
function aa_w3tc_minify_css_at_end_of_head() {
	if ( defined('W3TC') ) {
		echo "\n";
		echo "<!-- W3TC-include-css -->\n";
	}
}
add_action( 'wp_head', 'aa_w3tc_minify_css_at_end_of_head', 100 );

// Remove a ton of unecessary stuff from the head
function aa_remove_head_links() {
	remove_action( 'wp_head', 'wp_generator' );
	remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds
	remove_action( 'wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed
	remove_action( 'wp_head', 'rsd_link' ); // Display the link to the Really Simple Discovery service endpoint, EditURI link
	remove_action( 'wp_head', 'wlwmanifest_link' ); // Display the link to the Windows Live Writer manifest file.
	remove_action( 'wp_head', 'index_rel_link' ); // index link
	remove_action( 'wp_head', 'parent_post_rel_link', 10 ); // prev link
	remove_action( 'wp_head', 'start_post_rel_link', 10 ); // start link
	remove_action( 'wp_head', 'adjacent_posts_rel_link', 10 ); // Display relational links for the posts adjacent to the current post.
	remove_action( 'wp_head', 'wp_generator' ); // Display the XHTML generator that is generated on the wp_head hook, WP version
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );
}
add_action( 'init', 'aa_remove_head_links' );

// Improve the appearance of the_archive_title
function aa_custom_archive_title( $title ) {
	// Get amount of posts found
	global $wp_query;
	$found_number = "";
	if ( $wp_query ) $found_number = $wp_query->found_posts . " ";
	
	// Get post type as singular or plural depending on found number
	$o = get_post_type_object( get_post_type() );
	if ( $found_number == "1 " ) {
		$post_type = $o ? $o->labels->singular_name : "Entry";
	}else{
		$post_type = $o ? $o->labels->name : "Entries";
	}
	
	// Category/Tags/Taxonomies
	if( is_category() || is_tag() || is_tax() ) {
		$cat_title = single_cat_title( '', false );
		
		$verb = is_tax('tag') || is_tax('product_tag') ? 'tagged' : 'categorized under';
		
		// Displaying 5 posts categorized under Events
		// Displaying 5 events tagged Halloween
		return "Displaying $found_number $post_type $verb $cat_title";
	}
	
	// Author page
	if( is_author() ) {
		global $authordata;
		
		$author_name = apply_filters('the_author', is_object($authordata) ? $authordata->display_name : null);
		
		if ( $author_name ) {
			// Displaying 5 posts by Radley
			// Displaying 5 events by John Smith
			return "Displaying $found_number $post_type written by $author_name";
		}
	}
	
	if ( is_date() || is_month() || is_year() ) {
		// Always use plural post type name
		$post_type = $o ? $o->labels->name : "Entries";
		
		if ( is_date() || is_month() ) $range = get_the_time( 'F Y' );
		else if ( is_year() ) $range = get_the_time( 'F Y' );
		
		return ucwords($post_type) . ' from ' . $range;
	}
	
	return $title;
}
add_filter( 'get_the_archive_title', 'aa_custom_archive_title' );

// Allow shortcodes in widget text
add_filter( 'widget_text', 'do_shortcode' );

// Remove Empty Span in read more tags
function aa_remove_empty_read_more_span( $content ) {
	return preg_replace( "(<p><span id=\"more-[0-9]{1,}\"></span></p>)", "", $content );
}
add_filter( 'the_content', 'aa_remove_empty_read_more_span' );

// Improve wp_title. Obsolete if using yoast.
function aa_customize_wp_title( $title, $separator ) {
	if ( is_feed() ) return $title;
	if ( is_admin() ) return $title;
	
	global $paged, $page;
	
	if ( is_search() ) {
		
		//If we're a search, let's start over:
		$title = sprintf( __( 'Search results for %s', 'theme' ), '"' . get_search_query() . '"' );
		//Add a page number if we're on page 2 or more:
		if ( $paged >= 2 ) {
			$title .= " $separator " . sprintf( __( 'Page %s', 'theme' ), $paged );
		}
		//Add the site name to the end:
		$title .= " $separator " . get_bloginfo( 'name', 'display' );
		
		//We're done. Let's send the new title back to wp_title():
		return $title;
	}
	
	//Otherwise, let's start by adding the site name to the end:
	$title .= get_bloginfo( 'name', 'display' );
	
	//If we have a site description and we're on the home/front page, add the description:
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) ) {
		$title .= " $separator " . $site_description;
	}
	
	//Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 ) {
		$title .= " $separator " . sprintf( __( 'Page %s', 'theme' ), max( $paged, $page ) );
	}
	
	//Return the new title to wp_title():
	return $title;
}
add_filter( 'wp_title', 'aa_customize_wp_title', 10, 2 );

// Create a wp_nav_menu() fallback, to show a home link
function theme_page_menu_args( $args ) {
	$args['show_home'] = true;
	return $args;
}
add_filter( 'wp_page_menu_args', 'theme_page_menu_args' );