<?php
/**
 * Hide default welcome dashboard message and and create a custom one
 *
 * @access      public
 * @since       1.0 
 * @return      void
*/
function rc_my_welcome_panel() {
?>  

<!--styles generated by site options-->
<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

<script type="text/javascript">
/* Hide default welcome message */
jQuery(document).ready( function($) 
{
	$('div.welcome-panel-content').hide();
});
</script>
<style>
/*FIX ADMIN WELCOME PANEL AFTER WP 5.9 UPDATE*/
    .welcome-panel{
	background: linear-gradient(to bottom, rgba(0, 0, 0, 0.1), rgba(0, 0, 0, 0.1)), url(https://alchemyandaim.com/wp-content/uploads/2013/04/alchemy-and-aim-homepage-header.jpg) !important;
    padding:0 0 50px;
    }
    .welcome-panel .welcome-panel-column-container{
	    display:block;
	    background:transparent;
		margin-top:0;
    }
	.welcome-panel-column{
		display:block;
		float:left;
	}
	.index-php .welcome-panel:before{
		display:none;
	}
	.welcome-panel .preview-thumb:before{
		top: 12%;
	}
</style>
	
	<div class="welcome-panel-column-container">
    <div class="welcome-panel-column" style="width:47% !important;">
    <?php echo get_field('welcome_panel_text','options'); ?>
    <div class="sig"></div>
	</div>
    
    <div class="welcome-panel-column " style="width:50% !important;float:right;"> 
                   
    <div class="preview-thumb"> 
<?php
$ct = wp_get_theme();
$screenshot = $ct->get_screenshot();
$class = $screenshot ? 'has-screenshot' : '';
$customize_title = sprintf( __( 'Customize &#8220;%s&#8221;' ), $ct->display('Name') );
?>
	<?php if ( $screenshot ) : ?>
		<?php if ( current_user_can( 'edit_theme_options' ) ) : ?><br />
		<img src="<?php echo esc_url( $screenshot ); ?>" alt="<?php esc_attr_e( 'Current theme preview' ); ?>" />
		<?php endif; ?>
		<img class="hide-if-customize" src="<?php echo esc_url( $screenshot ); ?>" alt="<?php esc_attr_e( 'Current theme preview' ); ?>" />
	<?php endif; ?>
    </div>
    <div class="theme-specs"> 
	<h4>
		<?php echo $ct->display('Name'); ?>
	</h4>
	
		<ul class="theme-info">
			<li><?php printf( __('By %s'), $ct->display('Author') ); ?></li>
			<li><?php printf( __('Version %s'), $ct->display('Version') ); ?></li>
		</ul>
		<p><?php echo $ct->display('Description'); ?></p>
       
                    <a href="<?php echo home_url(); ?>" class="button-top" target="_blank">
                      <svg height="40" width="150">
                        <rect height="40" width="150" x="0" y="0" fill="none"></rect>
                      </svg>
                      <span>view your site</span>
                    </a>

        </div></div></div>
<?php
}
add_action( 'welcome_panel', 'rc_my_welcome_panel' );