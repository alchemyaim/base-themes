
jQuery(document).ready(function ($) {
	
	/* Show sub-menus when tabbing */
	$('#linkbar ul li ul.sub-menu a').on('focus', function(){
		$(this).parents('.sub-menu').addClass('active');
	});
	$('#linkbar ul li ul.sub-menu a').on('blur', function(){
		$(this).parents('.sub-menu').removeClass('active');
	});

	/* Slide-out menu */
	$('#nav-close, #slideout-trigger').on('click', function(){

		// Toggle .open class
		$('#nav-close, #slideout-trigger').toggleClass('open');

		// Check if open or closed
		if ( $(this).hasClass('open') ) {

			// Adjust aria
			$('#nav-close, #slideout-trigger').attr('aria-expanded','true');
			$('#slideout-menu').attr('aria-hidden','false');

			// Set Focus
			$('#slideout-menu ul a').eq(0).focus();

			// Set focus trap
			$('#nav-close').on('blur', function(){
				$('#slideout-menu ul a').eq(0).focus();
			});

		} else {

			// Adjust aria
			$('#nav-close, #slideout-trigger').attr('aria-expanded','false');
			$('#slideout-menu').attr('aria-hidden','true');

			// Set Focus
			$('#slideout-trigger').focus();

			// Remove focus trap
			$('#nav-close').off('blur');

		}
	});

	/* Show sub-menus in slide-out menu when tabbing */
	$('#slideout-menu ul.sub-menu a').on('focus', function(){
		$(this).parents('.sub-menu').addClass('active');
	});
	$('#slideout-menu ul.sub-menu a').on('blur', function(){
		$(this).parents('.sub-menu').removeClass('active');
	});

	$('#slideout-menu .menu-item-has-children').off('click').on('click', function(){
		$(this).toggleClass('toggled');

		// Set focus
		$(this).find('a').eq(0).focus();
	});

	/* Credits focus */
	$('#credit-trigger').on('click', function(){ 

		// Set focus
		$('#creditslide a').eq(1).focus();

		// Set focus trap
		$('#creditslide a:last-of-type').on('blur', function(){
			$('#credit-close').focus();
		});

		// ARIA
		if ( $('#credit-trigger').attr('aria-expanded') == 'false' ) {
			$('#credit-trigger, #credit-close').attr('aria-expanded','true');
			$('#creditslide').attr('aria-hidden','false');
		} else {
			$('#credit-trigger, #credit-close').attr('aria-expanded','false');
			$('#creditslide').attr('aria-hidden','true');
		}

	});

	$('#credit-close').on('click', function(){
		// Remove focus trap
		$('#creditslide a:last-of-type').off('blur');

		// ARIA
		$('#credit-trigger, #credit-close').attr('aria-expanded','false');
		$('#creditslide').attr('aria-hidden','true');
	});

	/* Set aria-label explaining external links */
	$('a[target="_blank"]').each(function(){

		var aria_label = $(this).attr('aria-label');

		// Check if aria-label exists
		if ( !aria_label ) {
			// Add aria-label
			$(this).attr('aria-label','This link opens a new window/tab');
		}

	});

	/* Add button role to links which act as buttons */
	$('a[href="#"], a[href="javascript:void(0)"]').attr('role','button');

	/* Wrap menus in <nav> element */
	$('.menu').each( function() {
		if ( $(this).parents('nav').length < 1 ) {
			$(this).wrap('<nav role="navigation"/>');
		}
	});

	/* Add aria-required attribute to required form fields */
	$('input[placeholder*="*"]').attr('aria-required','true');
	

});
