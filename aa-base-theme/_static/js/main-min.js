/*  -------------------------------------------------------
	Javascript helper document insert any simple JS functions here.
------------------------------------------------------- */





/*  -------------------------------------------------------------
		DEFAULT JS FUNCTIONS BELOW THIS LINE
------------------------------------------------------------- */
/*SLIDE OUT MENU*/	
jQuery(function($) {
		// Slideout Menu
	"use strict";
    $('#slideout-trigger').on('click', function(event){
    	event.preventDefault();
    	// create menu variables
    	var slideoutMenu = $('#slideout-menu');
    	var slideoutMenuWidth = $('#slideout-menu').outerWidth();

    	// toggle open class
    	slideoutMenu.toggleClass("open");

    	// slide menu
    	if (slideoutMenu.hasClass("open")) {
	    	slideoutMenu.css('display','block').animate({
		    	right: "0px"
	    	});
    	} else {
	    	slideoutMenu.animate({
		    	right: -slideoutMenuWidth
	    	}, 250, function(){
				slideoutMenu.delay(300).css('display','none');
			});
    	}
    });

		$('#nav-close').on('click', function(event){
			event.preventDefault();
			// create menu variables
    	var slideoutMenu = $('#slideout-menu');
    	var slideoutMenuWidth = $('#slideout-menu').outerWidth();

    	// toggle open class
    	slideoutMenu.toggleClass("open");

    	// slide menu
    	if (slideoutMenu.hasClass("open")) {
	    	slideoutMenu.css('display','block').animate({
		    	right: "0px"
	    	});
    	} else {
	    	slideoutMenu.animate({
		    	right: -slideoutMenuWidth
	    	}, 250, function(){
				slideoutMenu.delay(300).css('display','none');
			});
    	}
    });
	
	$('#content-wrapper').on('click', function(){
			// create menu variables
			var slideoutMenu = $('#slideout-menu');
			var slideoutMenuWidth = $('#slideout-menu').outerWidth();

			// toggle open class
			slideoutMenu.removeClass("open");

			// slide menu
			slideoutMenu.animate({
					right: -slideoutMenuWidth
				}, 250);
					
    });
	
	/*SLIDE OUT MENU DROPDOWN*/	
	$('#slideout-nav .sub-menu').before('<div id="submenu-link"></div>');
    $('#slideout-menu ul ul').hide();
    if ($('#slideout-menu .menu-item-has-children').length > 0) {
        $('#slideout-menu .menu-item-has-children').click(

        function () {
            $(this).addClass('toggled');
            if ($(this).hasClass('toggled')) {
                $(this).children('ul').slideToggle();
            }
            //return false;

        });
    }
});


//Gutenburg 'align-wide' & 'align-full' on pages with sidebars
jQuery(function($) {
	if ($('.sidebar').length) { 
		$('body').addClass('has-sidebar');
	} else {
		$('body').addClass('no-sidebar');
	}
});
  
//Scrolling Anchor Link
//var scroll = new SmoothScroll('a[href*="#"]');

//Move Text Below Mobile Image Flex Layout
jQuery(function($) {
	"use strict";
		$(".cls-mobile-image.before-txt").each(function() {
    		$(this).closest(".layout-section").find(".background_mobile-mobile-image-before .cls-inner").prepend(this);
		});
	});

//Fancy Credits
jQuery(function($) {
	"use strict";
	$("#creditslide").hide();
$("#credit-trigger").on( "click", function(e) {
		e.preventDefault();
		if ($('#credit-trigger').hasClass("credit-open") || $('#credit-close').hasClass("credit-open")) {
			$('#credit-trigger').removeClass("credit-open");
			$('#credit-close').removeClass("credit-open");
			$("#creditslide").removeClass("credit-open");

			setTimeout( function(){
				$("#creditslide").hide();
			}, 500);
		} else {
			$('#credit-trigger').addClass("credit-open");
			$('#credit-close').addClass("credit-open");
			$("#creditslide").show().addClass("credit-open");
		}
	});
	
	$("#credit-close").on( "click", function(e) {
		e.preventDefault();
		if ($('#credit-trigger').hasClass("credit-open") || $('#credit-close').hasClass("credit-open")) {
			$('#credit-trigger').removeClass("credit-open");
			$('#credit-close').removeClass("credit-open");
			$("#creditslide").removeClass("credit-open");
			setTimeout( function(){
				$("#creditslide").hide();
			}, 500);
		} else {
			$('#credit-trigger').addClass("credit-open");
			$('#credit-close').addClass("credit-open");
			$("#creditslide").show().addClass("credit-open");
		}
	});
	
});	

//Comment Form Validation
const requiredFields = document.querySelectorAll("[required]");
const form = document.querySelector(".js-form");
const summaryBox = document.createElement("div");
const summaryText = document.createElement("p");
const summaryList = document.createElement("ol");

const checkValidity = (event) => {
	event.preventDefault();
	const emptyFields = [];

	requiredFields.forEach((field) => {
		field.setAttribute('aria-invalid', 'false');

		// Check if field is empty
		if (!field.value) {
			field.setAttribute('aria-invalid', 'true');
			emptyFields.push(field.labels[0]);
		}
	});

	if (!emptyFields.length) {
		form.submit();
	} else {
		setSummary(emptyFields);
		setInlineMessage(emptyFields);
	}
};

const setSummary = (fields) => {
	summaryBox.classList.add("form__summary");
	summaryText.textContent =
		"There was a problem with your submission. The following inputs need attention";

	const listItems = fields.map((field) => {
		return `
			<li>
				<a href="#${field.getAttribute("for")}">${field.querySelector('.form__field-name').textContent}: ${field.dataset.validation}</a>
			</li>
		`;
	});

	summaryList.innerHTML = listItems.join("");

	summaryBox.append(summaryText, summaryList);
	summaryBox.setAttribute("tabindex", "0");
	form.prepend(summaryBox);
	summaryBox.focus();
};

const setInlineMessage = (fields) => {
	fields.forEach((field) => {
		const message = field.nextElementSibling.nextElementSibling;
		message.textContent = field.dataset.validation;
	});
};

if (form) {
form.addEventListener("submit", checkValidity);
}

//Accessibilty Enhancements
document.addEventListener("DOMContentLoaded", function () {
jQuery(function($) {
		//add 'aria-label to external links'	
		$("a[target='_blank']").each(function () {
		   var attr = $(this).attr('aria-label');
		   if (!attr) {
			//var txt = $(this).text();
			   var txt = "opens in a new window";
			$(this).attr("aria-label", txt);
		   }
		});
	
	$('.social-links .sr-only').each(function() {
        // Get the current element's text
        var text = $(this).text().trim();
        
        // Split text into words
        var words = text.split(' ');

        // Remove the last word
        words.pop();
        
        // Join words back into a string
        var modifiedText = words.join(' ');

        // Update the element's text content
        $(this).text(modifiedText);
    });
	
	});
});

//Focus Trap
function focusTrap(element, removeButton, handleClose) {
	const focusable =
		'button, a:not(.skiplink), input, select, textarea, [tabindex]:not([tabindex="-1"])';
	const focusableElements = element.querySelectorAll(focusable);
	const firstFocusableElement = focusableElements[0];
	const lastFocusableElement = focusableElements[focusableElements.length - 1];

	firstFocusableElement.focus();

	const shutdownFocusTrap = () => {
		handleClose();
		element.removeEventListener('keydown', handleKeydown);
		removeButton.removeEventListener('click', shutdownFocusTrap);
		removeButton.focus();
	};

	removeButton.addEventListener('click', shutdownFocusTrap);
	
	const handleKeydown = (event) => {
		const isEscPressed = (event.key === 'Escape');
		const isTabPressed = (event.key === 'Tab' || event.keyCode === 9);
		

		if ( isEscPressed ) {
			shutdownFocusTrap();
		}
		
		if ( !isTabPressed ) {
			return;
		}
		
		if ( event.shiftKey ) {
			if ( document.activeElement === firstFocusableElement ) {
				event.preventDefault();
				lastFocusableElement.focus();
			}
			return;
		}
		
		if ( document.activeElement === lastFocusableElement ) {
			event.preventDefault();
			firstFocusableElement.focus();
		}
	};
	
	element.addEventListener('keydown', handleKeydown);
}