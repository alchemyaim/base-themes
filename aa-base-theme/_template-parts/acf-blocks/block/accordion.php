<?php
/**
 * Block Name: Accordion Content
 *
 * This is the template that displays the ACF accordion content block.
 */

?>				
<div class="container">
	<?php
// Check rows existexists.
if( have_rows('accordion') ): ?>
<div class="accordion">

 <?php while( have_rows('accordion') ) : the_row();
	//var
    $label = get_sub_field('accordion_label');
	$content = get_sub_field('accordion_content');
  ?>
	<details class="accordion__details">
		<summary class="accordion__summary">
			<h3><span class="accordion__marker" aria-hidden="true"></span><?php echo $label; ?></h3>
		</summary>
				<?php echo $content; ?>
	</details>
  <?php  endwhile; ?>
	</div>
<?php else : ?>
	
<?php endif; ?>
</div>