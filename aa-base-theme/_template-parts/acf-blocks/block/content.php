<?php
/**
 * Block Name: General Content
 *
 * This is the template that displays the ACF general content block.
 */

?>				
<div class="container">
	<div class="content">
    	<?php echo get_field('content'); ?>
	</div>
</div>