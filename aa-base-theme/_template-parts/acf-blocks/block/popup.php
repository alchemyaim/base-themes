<?php
/**
 * Block Name: Popup Content
 *
 * This is the template that displays the ACF general content block.
 */

?>
<div class="container">
	<div class="content">
    	<?php echo get_field('content'); ?>
	</div>
</div>
<dialog class="dialog" aria-label="popup-modal">
	<div class="dialog__card">
		<button class="js-close-dialog"><span class="sr-only">Close</span></button>
    	<?php echo get_field('popup_modal_content'); ?>
	</div>
</dialog>

<script>
const buttonOpen = document.querySelector(".js-open-dialog");
const buttonClose = document.querySelector(".js-close-dialog");
const dialog = document.querySelector(".dialog");

const handleDialogClose = () => {
	document.documentElement.style.removeProperty("overflow");
	dialog.close();
};

const handleDialogOpen = () => {
	document.documentElement.style.overflow = "clip";
	dialog.showModal();
};

buttonOpen.addEventListener("click", handleDialogOpen);
buttonClose.addEventListener("click", handleDialogClose);
</script>