<?php
/**
 * Block Name: Slider Content
 *
 * This is the template that displays the ACF slider content block.
 */

?>				
<div class="container">
	<?php $section_id = str_replace('-', '_', strtolower(sanitize_title_with_dashes(get_field('section_label')))); ?>
	<?php $slider_type = get_field('slider_type'); ?>
	
	<?php if($slider_type == 'content') { ?>
	
	<?php if( have_rows('content_slider') ): ?>
	<div id="<?php echo $section_id; ?>" class="slider content-slider">
        <ul class="slider__slides" role="presentation" role="region" aria-live="polite">
	 <?php while( have_rows('content_slider') ) : the_row(); 
		//var
		$content = get_sub_field('slide_content');
	  ?>
			 <li class="slider__slide">
		<?php echo $content; ?>
			</li>
	  <?php  endwhile; ?>
			</ul>
    </div>
	<div class="slider__controls">
					<button data-action="previous" aria-label="Previous slide" >
						<svg width="24" height="10" viewBox="0 0 24 10" fill="none" xmlns="http://www.w3.org/2000/svg" role="presentation">
							<path
								d="M3.96967 1.18293C4.26256 0.890034 4.73744 0.890034 5.03033 1.18293C5.32322 1.47582 5.32322 1.95069 5.03033 2.24359L2.56066 4.71326H23.25C23.6642 4.71326 24 5.04904 24 5.46326C24 5.87747 23.6642 6.21326 23.25 6.21326H2.56066L5.03033 8.68293C5.32322 8.97582 5.32322 9.45069 5.03033 9.74359C4.73744 10.0365 4.26256 10.0365 3.96967 9.74359L0.219669 5.99359C0.0732231 5.84714 0 5.6552 0 5.46326C0 5.36156 0.0202389 5.2646 0.0569096 5.17617C0.0935097 5.08771 0.147762 5.00483 0.219669 4.93293L3.96967 1.18293Z"
								fill="currentColor"
							/>
						</svg>
					</button>
					<button data-action="next" aria-label="Next slide">
						<svg width="24" height="10" viewBox="0 0 24 10" fill="none" xmlns="http://www.w3.org/2000/svg" role="presentation">
							<path
								d="M20.0303 1.18293C19.7374 0.890034 19.2626 0.890034 18.9697 1.18293C18.6768 1.47582 18.6768 1.95069 18.9697 2.24359L21.4393 4.71326H0.75C0.335786 4.71326 0 5.04904 0 5.46326C0 5.87747 0.335786 6.21326 0.75 6.21326H21.4393L18.9697 8.68293C18.6768 8.97582 18.6768 9.45069 18.9697 9.74359C19.2626 10.0365 19.7374 10.0365 20.0303 9.74359L23.7803 5.99359C23.9268 5.84714 24 5.6552 24 5.46326C24 5.36156 23.9798 5.2646 23.9431 5.17617C23.9065 5.08771 23.8522 5.00483 23.7803 4.93293L20.0303 1.18293Z"
								fill="currentColor"
							/>
						</svg>
					</button>
				</div>
	<?php else : ?>
	<?php endif; ?>
	
	<?php } else if($slider_type == 'image') { ?> 
	<?php	//var
			$images = get_field('image_slider');
	  ?>
	 <div id="<?php echo $section_id; ?>" class="slider">
		<?php 
	if( $images ): ?>
   
        <ul class="slider__slides" role="presentation" role="region" aria-live="polite">
            <?php foreach( $images as $image ): ?>
                <li class="slider__slide">
                    <img class="slider__image" src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                </li>
            <?php endforeach; ?>
        </ul>
   
   <div class="slider__footer">
        <div class="slider__nav" aria-label="Navigate slides" role="tablist" aria-orientation="horizontal">
            <?php foreach( $images as $image ): ?>
               <button aria-label="slide 1" aria-selected="true" role="tab">
						<img src="<?php echo esc_url($image['sizes']['thumbnail']); ?>" alt="Thumbnail of <?php echo esc_url($image['alt']); ?>" />
				</button>
            <?php endforeach; ?>
	   </div>
    
	<div class="slider__controls">
					<button data-action="previous" aria-label="Previous slide" >
						<svg width="24" height="10" viewBox="0 0 24 10" fill="none" xmlns="http://www.w3.org/2000/svg" role="presentation">
							<path
								d="M3.96967 1.18293C4.26256 0.890034 4.73744 0.890034 5.03033 1.18293C5.32322 1.47582 5.32322 1.95069 5.03033 2.24359L2.56066 4.71326H23.25C23.6642 4.71326 24 5.04904 24 5.46326C24 5.87747 23.6642 6.21326 23.25 6.21326H2.56066L5.03033 8.68293C5.32322 8.97582 5.32322 9.45069 5.03033 9.74359C4.73744 10.0365 4.26256 10.0365 3.96967 9.74359L0.219669 5.99359C0.0732231 5.84714 0 5.6552 0 5.46326C0 5.36156 0.0202389 5.2646 0.0569096 5.17617C0.0935097 5.08771 0.147762 5.00483 0.219669 4.93293L3.96967 1.18293Z"
								fill="currentColor"
							/>
						</svg>
					</button>
					<button data-action="next" aria-label="Next slide">
						<svg width="24" height="10" viewBox="0 0 24 10" fill="none" xmlns="http://www.w3.org/2000/svg" role="presentation">
							<path
								d="M20.0303 1.18293C19.7374 0.890034 19.2626 0.890034 18.9697 1.18293C18.6768 1.47582 18.6768 1.95069 18.9697 2.24359L21.4393 4.71326H0.75C0.335786 4.71326 0 5.04904 0 5.46326C0 5.87747 0.335786 6.21326 0.75 6.21326H21.4393L18.9697 8.68293C18.6768 8.97582 18.6768 9.45069 18.9697 9.74359C19.2626 10.0365 19.7374 10.0365 20.0303 9.74359L23.7803 5.99359C23.9268 5.84714 24 5.6552 24 5.46326C24 5.36156 23.9798 5.2646 23.9431 5.17617C23.9065 5.08771 23.8522 5.00483 23.7803 4.93293L20.0303 1.18293Z"
								fill="currentColor"
							/>
						</svg>
					</button>
				</div>
		 </div>
<?php endif; ?>
	 
	
	<?php } ?>
	</div>
</div>

<script>
const slider_<?php echo $section_id; ?> = document.querySelector("#<?php echo $section_id; ?>.slider");
const slides_<?php echo $section_id; ?> = [...document.querySelectorAll("#<?php echo $section_id; ?> .slider__slide")];
const track_<?php echo $section_id; ?> = document.querySelector("#<?php echo $section_id; ?> .slider__slides");

const sliderNav_<?php echo $section_id; ?> = document.querySelector('#<?php echo $section_id; ?> .slider__nav');
const sliderTabs_<?php echo $section_id; ?> = document.querySelectorAll('#<?php echo $section_id; ?> .slider__nav > [role="tab"]');

const controls_<?php echo $section_id; ?> = document.querySelectorAll('#<?php echo $section_id; ?> [data-action]');

let scrollTotal_<?php echo $section_id; ?> = track_<?php echo $section_id; ?>.scrollWidth;
let scrollPart_<?php echo $section_id; ?> = scrollTotal_<?php echo $section_id; ?> / slides_<?php echo $section_id; ?>.length;

let currentSlide_<?php echo $section_id; ?> = 0;
let scrollTimer_<?php echo $section_id; ?> = null;

const handleResize_<?php echo $section_id; ?> = () => {
	scrollTotal_<?php echo $section_id; ?> = track_<?php echo $section_id; ?>.scrollWidth;
	scrollPart_<?php echo $section_id; ?> = scrollTotal_<?php echo $section_id; ?> / slides_<?php echo $section_id; ?>.length;
};

const scrollFinished_<?php echo $section_id; ?> = () => {
	scrollTimer_<?php echo $section_id; ?> = null;
};

const selectCurrentSlide_<?php echo $section_id; ?> = () => {
	sliderTabs_<?php echo $section_id; ?>.forEach((button, i) => {
		if (i === currentSlide_<?php echo $section_id; ?>) {
			button.setAttribute("aria-selected", "true");
		} else {
			button.setAttribute("aria-selected", "false");
		}
	});
	
	slides_<?php echo $section_id; ?>.forEach((slide, i) => {
		if (i === currentSlide_<?php echo $section_id; ?>) {
			slide.setAttribute("aria-hidden", "false");
		} else {
			slide.setAttribute("aria-hidden", "true");
		}
	});
};

const scrollToCurrentSlide_<?php echo $section_id; ?> = () => {
	clearTimeout(scrollTimer_<?php echo $section_id; ?>);

	scrollTimer_<?php echo $section_id; ?> = setTimeout(() => {
		scrollFinished_<?php echo $section_id; ?>();
	}, 700);

	track_<?php echo $section_id; ?>.scrollLeft = currentSlide_<?php echo $section_id; ?> * scrollPart_<?php echo $section_id; ?>;
}

controls_<?php echo $section_id; ?>.forEach(control => {
	control.addEventListener("click", event => {
		const button = event.target.closest('button');
		const { action } = button.dataset;

		if (action === 'previous') {
			if (currentSlide_<?php echo $section_id; ?> === 0) {
				currentSlide_<?php echo $section_id; ?> = slides_<?php echo $section_id; ?>.length - 1;
			} else {
				currentSlide_<?php echo $section_id; ?>--;
			}
		}

		if (action === 'next') {
			if (currentSlide_<?php echo $section_id; ?> === slides_<?php echo $section_id; ?>.length - 1) {
				currentSlide_<?php echo $section_id; ?> = 0;
			} else {
				currentSlide_<?php echo $section_id; ?>++;
			}
		}

		scrollToCurrentSlide_<?php echo $section_id; ?>()
		selectCurrentSlide_<?php echo $section_id; ?>();
	})
})

sliderTabs_<?php echo $section_id; ?>.forEach( (tab, i) => {
	tab.addEventListener('click', () => {
		currentSlide_<?php echo $section_id; ?> = i;

		selectCurrentSlide_<?php echo $section_id; ?>();
		scrollToCurrentSlide_<?php echo $section_id; ?>();
	})
})

sliderNav_<?php echo $section_id; ?>.addEventListener('keydown', event => {
	if (event.key === "Tab") {
		event.preventDefault();
		controls_<?php echo $section_id; ?>[0].focus();
		return;
	}

	if (event.key !== "ArrowRight" && event.key !== "ArrowLeft") {
		return;
	}

	if (event.key === "ArrowRight") {
		currentSlide_<?php echo $section_id; ?>++;

		if (currentSlide_<?php echo $section_id; ?> >= sliderTabs_<?php echo $section_id; ?>.length) {
			currentSlide_<?php echo $section_id; ?> = 0;
		}
	}

	if (event.key === "ArrowLeft") {
		currentSlide_<?php echo $section_id; ?>--;

		if (currentSlide_<?php echo $section_id; ?> < 0) {
			currentSlide_<?php echo $section_id; ?> = sliderTabs_<?php echo $section_id; ?>.length - 1;
		}
	}

	sliderTabs_<?php echo $section_id; ?>[currentSlide_<?php echo $section_id; ?>].focus();
	selectCurrentSlide_<?php echo $section_id; ?>();
	scrollToCurrentSlide_<?php echo $section_id; ?>();
})

track_<?php echo $section_id; ?>.addEventListener("scroll", () => {
	if (scrollTimer_<?php echo $section_id; ?>) {
		return;
	}

	const fraction = Math.round(track_<?php echo $section_id; ?>.scrollLeft / scrollPart_<?php echo $section_id; ?>);

	if (fraction !== currentSlide_<?php echo $section_id; ?>) {
		currentSlide_<?php echo $section_id; ?> = fraction;
		selectCurrentSlide_<?php echo $section_id; ?>();
	}
});

window.addEventListener("resize", handleResize_<?php echo $section_id; ?>);
selectCurrentSlide_<?php echo $section_id; ?>();

</script>