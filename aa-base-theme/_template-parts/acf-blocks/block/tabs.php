<?php
/**
 * Block Name: Tabbed Content
 *
 * This is the template that displays the ACF tabbed content block.
 */

?>				
<div class="container">
	<?php $section_id = str_replace('-', '_', strtolower(sanitize_title_with_dashes(get_field('section_label')))); ?>
	<div id="<?php echo $section_id; ?>" class="tabs">
		
	<?php if( have_rows('tabs') ): $i = 0; ?>
<div role="tablist">
 <?php while( have_rows('tabs') ) : $i++; the_row();
	//var
  	$tab_btn = get_sub_field('tab_button');
  ?>
<button role="tab" id="<?php echo $section_id; ?>_tab_<?php echo $i; ?>" aria-selected="false" aria-controls="<?php echo $section_id; ?>_panel_<?php echo $i; ?>" tabindex="0"><?php echo $tab_btn; ?></button>
<?php  endwhile; ?>
	</div>
<?php else : ?>
<?php endif; ?>
		
<?php if( have_rows('tabs') ): $i = 0; ?>
 <?php while( have_rows('tabs') ) : $i++; the_row();
	//var
	$tab_content = get_sub_field('tab_panel_content');
  ?>
	<div role="tabpanel" id="<?php echo $section_id; ?>_panel_<?php echo $i; ?>" aria-labelledby="<?php echo $section_id; ?>_tab_<?php echo $i; ?>" aria-hidden="true">
		<?php echo $tab_content; ?>
	</div>
<?php  endwhile; ?>
<?php else : ?>
<?php endif; ?>

	</div>
</div>

<script>
jQuery(function($) {
	$('#<?php echo $section_id; ?> div[id*="panel_1"]').attr('aria-hidden', 'false');
	$('#<?php echo $section_id; ?> button[id*="tab_1"]').attr('aria-selected', 'true');
});
	
const component_<?php echo $section_id; ?> = document.querySelector('#<?php echo $section_id; ?>.tabs');
const tabList_<?php echo $section_id; ?> = component_<?php echo $section_id; ?>.querySelector('#<?php echo $section_id; ?> [role="tablist"]');
const tabs_<?php echo $section_id; ?> = component_<?php echo $section_id; ?>.querySelectorAll('#<?php echo $section_id; ?> [role="tab"]');
const tabPanels_<?php echo $section_id; ?> = component_<?php echo $section_id; ?>.querySelectorAll('#<?php echo $section_id; ?> [role="tabpanel"]');

let tabsIndex_<?php echo $section_id; ?> = 0;

tabs_<?php echo $section_id; ?>.forEach((tab) => {
	tab.addEventListener("click", () => {
		changeTabs_<?php echo $section_id; ?>(tab);
	});
});


tabList_<?php echo $section_id; ?>.addEventListener("keydown", event => {
	if (event.key !== "ArrowRight" && event.key !== "ArrowLeft") {
		return;
	}

	tabs_<?php echo $section_id; ?>[tabsIndex_<?php echo $section_id; ?>].setAttribute("tabtabsIndex", -1);

	if (event.key === "ArrowRight") {
		tabsIndex_<?php echo $section_id; ?>++;

		if (tabsIndex_<?php echo $section_id; ?> >= tabs_<?php echo $section_id; ?>.length) {
			tabsIndex_<?php echo $section_id; ?> = 0;
		}
	}

	if (event.key === "ArrowLeft") {
		tabsIndex_<?php echo $section_id; ?>--;

		if (tabsIndex_<?php echo $section_id; ?> < 0) {
			tabsIndex_<?php echo $section_id; ?> = tabs.length - 1;
		}
	}

	tabs_<?php echo $section_id; ?>[tabsIndex_<?php echo $section_id; ?>].setAttribute("tabtabsIndex", 0);
	tabs_<?php echo $section_id; ?>[tabsIndex_<?php echo $section_id; ?>].focus();
	changeTabs_<?php echo $section_id; ?>(tabs_<?php echo $section_id; ?>[tabsIndex_<?php echo $section_id; ?>]);
});

function changeTabs_<?php echo $section_id; ?>(nextTab) {
	tabs_<?php echo $section_id; ?>.forEach( tab => {
		if (tab === nextTab) {
			tab.setAttribute("aria-selected", true)
		} else {
			tab.setAttribute("aria-selected", false);
		}
	});

	tabPanels_<?php echo $section_id; ?>.forEach( tabPanel => {
		tabPanel.setAttribute("aria-hidden", true)
	});

	component_<?php echo $section_id; ?>.querySelector(`#${nextTab.getAttribute("aria-controls")}`).setAttribute("aria-hidden", false);
}

</script>