<?php
if ( !isset($section) ) die('Must be accessed through flex-layout.php');

// Fields used by this section with default values
$f = shortcode_atts(array(
	'content_appearance' => null,
	'content' => null,
), $section, 'custom-layout');

// Extra classes to use on the container
$classes = array();
$classes[] = 'appearance-' . $f['content_appearance'];

// Render the section
aa_flex_layout_section_start($section, $i, $classes);

?>
<div class="container">
	<?php
	echo $f['content'];
	?>
</div>
<?php

aa_flex_layout_section_end();