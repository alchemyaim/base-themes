<article <?php post_class( 'entry entry-archive' ); ?>>
	
	<?php if ( has_post_thumbnail() ) {
	 $thumbnail_id = get_post_thumbnail_id();
	$title = get_the_title();
	$alt = [ 'alt' => 'Link to '.$title ];
    $image = wp_get_attachment_image( $thumbnail_id, 'full', false, $alt );  ?>
		<div class="entry-image">
			<a href="<?php the_permalink(); ?>"><?php  echo $image;?></a>
		</div>
	<?php } ?>
	
	<header class="entry-header">
		<?php the_title( '<h3 class="entry-title">', '</h3>' ); ?>
	</header>
	
	<?php get_template_part( '_template-parts/post-meta' ); ?>
	
	<div class="entry-content">
		<?php the_excerpt(__('new_excerpt_length')); ?>
	</div>

	<div class="entry-read-more">
		<a href="<?php the_permalink(); ?>" class="button alt" aria-label="Read More About <?php echo the_title(); ?>">Read More <span class="sr-only">About <?php echo the_title(); ?></span></a>
	</div>

</article>