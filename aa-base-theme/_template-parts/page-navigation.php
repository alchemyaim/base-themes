<div id="navigation">
	<?php if ( is_singular() ) : ?>
		
		<div class="pagination pagination-singular">
			<?php if ( get_next_post() ) { ?>
				<div class="nav-next"><?php next_post_link( '%link', 'Next Post' ) ?></div>
			<?php } ?>
			
			<?php if ( get_previous_post() ) { ?>
				<div class="nav-previous"><?php previous_post_link( '%link', 'Previous Post' ) ?></div>
			<?php } ?>
		</div>
	
	<?php else : ?>
		
		<div class="pagination pagination-archive">
			<?php if ( get_next_posts_link() ) { ?>
				<div class="nav-next"><?php next_posts_link( 'Older Posts' ) ?></div>
			<?php } ?>
			
			<?php if ( get_previous_posts_link() ) { ?>
				<div class="nav-previous"><?php previous_posts_link( 'Newer Posts' ) ?></div>
			<?php } ?>
		</div>
	
	<?php endif; ?>
</div>