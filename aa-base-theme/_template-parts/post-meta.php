<div class="post-meta">Written by <a href="<?php echo get_author_posts_url(get_the_author_meta( 'ID' )); ?>">
    <?php the_author(); ?>
      </a> under <?php the_category(' , '); ?> on <span class="post-date">
      <?php the_time('F j, Y'); ?>
      </span> &ensp;|&ensp;
      <?php comments_number(__('No Comments')); ?>
</div>