<?php if ( is_single() ) : ?>
<div class="post-footer">
	<div class="share-buttons">
     <?php 
		$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
		$subject =  get_the_title();
		$body = get_permalink();
		$desc = get_the_excerpt();
		
	?>
		<div class="tshare"><a href="https://twitter.com/intent/tweet?text=Currently reading: <?php the_title ();?>&url=<?php echo get_settings('home'); ?>/?p=<?php the_ID(); ?>" target="_blank"><i class="fa fa-brands fa-x-twitter"></i> <span class="sr-only">Share this post on</span> Twitter</a></div>
       
       <div class="fshare"><a href="https://www.facebook.com/dialog/feed?app_id=815404179098514&link=<?php the_permalink() ?>&picture=<?php echo $feat_image; ?>&name=<?php echo the_title (); ?>&caption=%20&description=<?php echo $desc; ?>&redirect_uri=http%3A%2F%2Fwww.facebook.com%2F" target="_blank"><i class="fa fa-brands fa-facebook-f"></i> <span class="sr-only">Share this post on</span> Facebook</a></div>
       
       <div class="pshare"><a href="//www.pinterest.com/pin/create/button/?url=<?php the_permalink() ?>&media=<?php echo $feat_image; ?>&description=<?php the_title(); ?> - <?php echo $desc; ?>" data-pin-do="buttonBookmark" target="_blank" ><i class="fa fa-brands fa-pinterest-p"></i> <span class="sr-only">Share this post on</span> Pinterest</a></div>
       
       <div class="comment-link"><a href="<?php the_permalink(); ?>/#respond"><i class="fa fa-comment" aria-hidden="true"></i> <span class="sr-only">Leave a</span> Comment</a> <span class="sr-only">for this post</span></div>
      
    </div>
</div>
<?php endif; ?>