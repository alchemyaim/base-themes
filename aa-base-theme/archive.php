<?php
if ( !have_posts() ) {
	// If no posts match the query
	get_template_part( '404' );
	return;
}

get_header();
?>
	<div class="container">
		<div class="content-area">
			<?php if (is_home() ) { ?>
			<h1 class="archive-header">Blog</h1>
			<?php } else { ?>
			<h1 class="archive-header"><?php the_archive_title(); ?></h1>
			<?php } ?>

			<h2 class="sr-only">Posts</h2>
			
			<?php
			while( have_posts() ): the_post();
				get_template_part( '_template-parts/loop-archive', get_post_type() );
			endwhile;
			?>
			
			<?php get_template_part( '_template-parts/page-navigation' ); ?>
		</div>
		
		<div class="aside">
			<h2 class="sr-only">Sidebar</h2>
			<?php get_sidebar(); ?>
		</div>
	</div>
<?php
get_footer();