<?php
// Do not access this file directly
if ( !defined('ABSPATH') ) exit;

// If passworded, must have given password
if ( post_password_required() ) return;

// If comments are closed and there are no comments already, do not show comments at all
if ( !comments_open() && !get_comments_number() ) return;
?>
<div id="comments">
	
	<h2>
		<?php comments_number( '0 comments to', '1 comment to', '% comments to' ); ?>
		"
		<?php the_title(); ?>
		"</h2>
	<?php if ( have_comments() ) { ?>
		<ul class="comment-block" id="comment-block">
			<?php
			// Callback defined in /_includes/functions/theme-functions.php
			wp_list_comments( 'callback=aa_list_comments' );
			?>
		</ul>
	<?php } ?>
	
	<!-- Comment Form -->
	<?php if ( 'open' == $post->comment_status ) : ?>
	
		<div id="respond">
			
			<h3 class="comments-headers"><?php comment_form_title( 'Leave a Comment', 'Leave a Reply to %s' ); ?>
				<span id="cancel-comment-reply"><?php cancel_comment_reply_link( '(cancel)' ) ?></span></h3>
			
			<?php if ( get_option( 'comment_registration' ) && !$user_ID ) : ?>
				
				<p class="unstyled">You must <a href="<?php echo get_option( 'siteurl' ); ?>/wp-login.php?redirect_to=<?php the_permalink(); ?>">log in</a> to post a comment.</p>
			
			<?php else : ?>
				
				<form class="form js-form" novalidate action="<?php echo get_option( 'siteurl' ); ?>/wp-comments-post.php" method="post" id="comment_form">
					<fieldset class="form__data">
					<?php if ( $user_ID ) { ?>
						<p class="unstyled">Logged in as <a href="<?php echo get_option( 'siteurl' ); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>.
							<a href="<?php echo wp_logout_url( get_permalink() ); ?>" title="<?php _e( 'Log out of this account' ) ?>">Logout &raquo;</a></p>
					<?php } ?>
					<?php if ( !$user_ID ) { ?>
						<div class="form__name form-element">
							<label for="author" data-validation="We need your name to address you correctly">
								<span class="form__field-name">First name</span> <span aria-hidden="true" class="form__required">(required)</span>
							</label>
							<input aria-describedby="name-validation" required autocomplete="given-name" class="u-full-width" type="text" name="author" id="author" value="<?php echo $comment_author; ?>" />
							<small id="name-validation" class="form__validation"></small>
						</div>
						<div class="form__email form-element">
							<label for="email" data-validation="We need your email address to be able to send you reply notifications">
								<span class="form__field-name">Email address</span> <span aria-hidden="true" class="form__required">(required)</span>
							</label>
							<input aria-describedby="email-validation" required autocomplete="email" class="u-full-width" type="text" name="email" id="email" value="<?php echo $comment_author_email; ?>" />
							<small id="email-validation" class="form__validation"></small>
						</div>
						<div class="form__url form-element">
							<label for="url">
								<span class="form__field-name">Website</span>
							</label>
							<input autocomplete="url" class="u-full-width" type="text" name="url" id="url" value="<?php echo $comment_author_url; ?>" />
						</div>
					<?php } ?>
						<div class="form_comment form-element">
							<label for="comment" data-validation="We need your comment to this post">
								<span class="form__field-name">Comment</span> <span aria-hidden="true" class="form__required">(required)</span>
							</label>
							<textarea aria-describedby="comment-validation" required autocomplete="on" class="u-full-width" name="comment" id="comment" rows="8"></textarea>
							<small id="comment-validation" class="form__validation"></small>
						</div>
					<?php if ( function_exists( 'show_subscription_checkbox' ) ) {
						show_subscription_checkbox();
					} ?>
					<?php comment_id_fields(); ?>
						<button class="button button-primary" type="submit" id="btnSubmit" >Submit</button>
					<?php do_action( 'comment_form', $post->ID ); ?>
					</fieldset>
				</form>
			
			<?php endif; // If registration required and not logged in ?>
		
		</div>
	
		
		<div class="pagination pagination-comments">
			<div class="nav-previous"><?php previous_comments_link( 'older comments' ) ?></div>
			<div class="nav-next"><?php next_comments_link( 'newer comments' ) ?></div>
		</div>
	
	<?php endif; // if you delete this the sky will fall on your head ?>

</div><?php /* #comments */ ?>
<div style="clear:both;"></div>
