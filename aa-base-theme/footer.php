<div style="clear:both;"></div>

</div><!--content-wrapper-->

</main>

<footer>

	<div id="credits" class="container">
		<div class="footer-left u-pull-left"><?php if ( is_active_sidebar( 'footer_left' ) ) dynamic_sidebar( 'footer_left' ); ?></div>
		<div class="footer-right u-pull-right"><?php if ( get_field( 'enable_fancy_credits', 'option' ) == 1 ) { echo do_shortcode(get_field( 'standard_site_credits', 'option' )); ?> <a href="#" id="credit-trigger" tabindex=0 aria-expanded="false"><?php echo get_field( 'fancy_credit_trigger_text', 'option' ); ?></a><?php } else { echo do_shortcode(get_field( 'standard_site_credits', 'option' )); } ?></div>
	</div>

	<?php if ( get_field( 'enable_fancy_credits', 'option' ) == 1 ) { ?>
		<div id="creditslide" aria-hidden="true">
			<a id="credit-close" href="#" aria-expanded="false"></a>
			<div class="container">
		<?php if ( have_rows( 'fancy_credits', 'option' ) ) : ?>
			<?php while ( have_rows( 'fancy_credits', 'option' ) ) : the_row(); ?>
				<div class="callout-std">
					<span><?php echo get_sub_field( 'credit_title' ); ?></span>
				<?php $credit_link = get_sub_field( 'credit_link' ); ?>
				<?php if ( $credit_link ) { ?>
					<a href="<?php echo $credit_link['url']; ?>" target="<?php echo $credit_link['target']; ?>"><?php echo $credit_link['title']; ?></a>
				<?php } ?>
					</div>
			<?php endwhile; ?>
		<?php else : ?>
			<?php // no rows found ?>
		<?php endif; ?>
				</div>
		</div>
	<?php } ?>
</footer>

<?php wp_footer(); ?>

<?php //Google Analytics
if ( get_field( 'google_analytics_location', 'option' ) == "footer" ) {
	echo get_field( 'google_analytics_code', 'option' );
}
?>

</body>
</html>