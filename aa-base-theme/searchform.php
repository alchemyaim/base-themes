<form method="get" id="search_form" action="<?php bloginfo( 'home' ); ?>/">
	<fieldset class="form_data">
		<label for="search">Search</label>
		<input type="text" aria-label="Search" class="search_input" value="Search..." name="s" id="s" onfocus="if (this.value == 'Search...') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Search...';}" />
		<input type="submit" id="searchsubmit" value="Submit" />
	</fieldset>
</form>